console.log("Hello World")

// Assignment Operator
let assignmentNumber = 8;

// Arithmetic Operators
// + - * / %


// ADDITIONAL ASSIGNMENT OPERATOR(+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10
// shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

// Subtraction/Multiplication/Division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *=2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

// Muliple Operators and Parenthesis
/*
	-when mulptiple operators are applied in a single statement,  it follows th PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) Rule
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 -3) * (4 / 5);
console.log(pemdas);

// Increment and Decrement Operators
// Operators that add or subtract values by 1 and reassigns the value  of the  variable where the increment/decrement was applied to

let z = 1;

// increment
//pre-fix incrementation
// The value of "z" is added by a value of 1 before returning  the value and storing it in the variable "increment"
++z;
console.log(z); //2 - the value of z was added with 1 and 	is immediately  returned.

 //post-fix incrementation
 // The value of "z" is returned and stored in the variable "increment" then the value of 'z' is increased by one
 z++;
 console.log(z); //3 - the value of z was added with 1 
console.log(z++); //3 - the previous value of the variable is returned.
console.log(z); //4 - a new value is now returned

// prefix vs post-fix incrementation
console.log(z++); //4
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately

// prefix vs post-fix incrementation
console.log(--z); //5 with pre-fix decrementation the result of substruction  by 1 is returned immediately

console.log(z--); //5 the result of substruction by 1 is not immediately returned, instead the previous value is returned first
console.log(z); //4 


// TYPE COERCION
// is the automatic or implicit conversion of values from one data type to another
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);
// Adding/Concatinating a string and a number will result to  a string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// the boolean "true" is also associated with  the value 1
let numE = true + 1;
console.log(numE); //2

// the boolean "false" is also associated with  the value 0
let numF = false + 1;
console.log(numF);
console.log(typeof numF); //1


// COMPARIZON OPERATORS

// (==) Equality Operator
// checks whether the operans are equal/have the same content
// Attempts to CONVERT AND COMPARE operands of different data types
// Returns a boolean value

console.log(1==1); //true
console.log(1==2); //false
console.log(1=='1'); //true
console.log(0==false); //true
console.log('juan'=='JUAN'); //false, case sensitive
// console.log('juan'== juan);


// (!=) Inequality Operator
// checks whether the operands are not equal/have different content
let juan = null

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //
console.log('juan' != 'JUAN'); //
console.log('juan' != juan); //


// (===) Strictly Equality Operator
console.log('Strictly Equality Operator')
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 ==='1'); //false =not the same data type
console.log(0 === false); //false = different data type
console.log('juan'==='JUAN'); //false, case sensitive
// console.log('juan'=== juan); //false

// (!==) Strictly Inequality Operator
console.log('Strictly Inequality Operator')
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN'); //true
console.log('juan' !== juan); //true



// RELATIONAL COMPARIZON OPERATORS
// check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500";


console.log('Greater than');
// Greater than (>)
console.log(x > y); //false
console.log(w > y); //true


console.log('less than');
// Less than (<)
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000); //true
console.log(numString < 1000); //false - forced coercion
console.log(numString < 6000); //true - forced coercion to change string to number
console.log(numString < 'jose'); //true - "5500" < "jose"

// Greater than oe Equal to
console.log(w >= 10000); //false
console.log(w >= 8000); //true

// Less than oe Equal to
console.log(x <= 7); //true
console.log(y <= y); //true

// Logical Operators
let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("Logical AND Operators")
// logical AND Operator (&& - Double Ampersand)
// Returns true if all operands are true
let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //

let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false

let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization4); //true


let userName = "gamer2022";
let userName2 = "shadow1991";
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
// .length is a property of strings which determine the number of character in the string.
console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true

console.log("Logical OR Operators")
// OR operator (|| - Double Pipe)
// returns true if atleast ONE of the operands are true

let useLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1); //true

let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(registration2); //false

console.log("NOT Operator")
// NOT Operator (!)
// turns a boolean into the opposite value

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //true


console.log(!isRegistered); //false
console.log(!isLegalAge); //false


let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite); //true - is Admin  original  value = false
console.log(opposite2);//false - is Admin  original  value = true


// if, else if, and else statement

// IF statement
// if statement will run a code block if the condition is specified is true or results to true

// if(true){
//     alert("We just run an if condition");
// }


let numG = -1;

if(numG < 0) {
    console.log('Hello');
}

let userName3 = "crusader_1993";
let userLevel3 =3;
let userAge3 = 20;

if(userName3.length > 10){
    console.log("Welcome to Game Online!")
}
if(userLevel3 >= requiredLevel){
    console.log("You are qualified to join the guild!")
}
if(userName3.length >= 10 && isRegistered && isAdmin){
    console.log("Thank you for joining the Admin!")
} else {
    console.log("You are not ready to be an Admin")
}

// ELSE statement
// The "else" statement execute a block of codes if all other conditions are false

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
    console.log("Thank you for joining th Noobies Giuld!")
} else {
    console.log("You are too strong to be a noob!")
}

// else if statement
// else if execute a statement if the previous of the origin  condition is false or resulted  to false but another specified conditon resulted to one.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
    console.log("Thank you for joining the noobies guild")
} else if(userAge3 > 25){
    console.log("You are too strong to be a noob!")
} else if(userAge3 < requiredAge){
    console.log("you are too young to join the guild")
} else{
    console.log("better luck next time")
}


// If, else if and else statement with functions

function addNum(num1, num2) {
    // check if the numbers being pass are number types.
    // typeof keyword returns a string which tells the types of data  that follows it.
    if(typeof num1 === "number" && typeof num2 === "number"){console.log("Run only if both arguments passed are number types");
        console.log(num1 + num2);
    } else {
        console.log("One of both of the arguments are not numbers");
    }
}

addNum(5, 2);


// create log in function

function login(userName, password) {
    // check if the argument passed are strings
    if(typeof userName === "string" && typeof password === "string"){
        console.log("Both Arguments are Strings.")
        /*
        nested if-else
            will run if the parent if statement is able  to agree to accomplish its condition
        */
        if (userName.length >= 8 && password.length > 8){
            console.log("Thank you for logging in")
        } 
        else if(userName.length <= 8){
            console.log("Username is too short")
            }
        else if (password.length <= 8){
            console.log("password too short")
            }
        else{
            console.log("Credential too short")
            }
        }


     else {
        console.log("One of the argument is not string.")
    }
}


login("janeda", "123as");


// function with return keyword

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if(windSpeed < 30){
        return "Not a typhoon yet.";
    }
    else if(windSpeed <= 61) {
        return "Tropical depression detected.";
    }
    else if(windSpeed >=62 && windSpeed <= 88) {
        return "Tropical storm detected.";
    }
    else if(windSpeed >= 89 && windSpeed <= 117) {
        return "Severe tropical storm detected."
    }
    else {
        return "Typhoon detected."
    }
}

message = determineTyphoonIntensity(68);
console.log(message);

// console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
if (message == "Tropical storm detected."){
    console.warn(message);
}


// Truthy and Falsy

// Truthy
if (true) {
    console.log('Truthy')
}

if (1){
    console.log('True')
}

if([]) {
    console.log('Truthy')
}

// falsy
// -0,"", nul, NaN
if(false){
    console.log('falsy')
}

if(0){
    console.log('falsy')
}

if(undefined) {
    console.log('falsy')
}


// Ternary Operator

/*
    Syntax:
        (expression/condition) ? ifTrue : ifFalse;
    Three operand of ternary operator:
    1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is false

*/


// Ternary operators were created so that we can have an if-else statement in one  line

// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log(`Result of ternary operator ${ternaryResult}`);

if(1 < 18) {
    console.log(true)
} 
else {
    console.log(false)
}

let villain = "Harvey Dent"

villain === "Two  Face"
? console.log ("You lived long enough to be a villain.")
: console.log("Not quite villainous yet.")

// Ternary operators have an implicit "return" statement that  without return keyword the resulting expression can be stored in a variable.